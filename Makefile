# The main Makefile is located in '/src/'. The purpose of
# this Makefile is to provide accessability and allow the
# GitLab CI tests to function properly.

SRC_DIR = src

all:
	cd $(SRC_DIR) && $(MAKE)

.PHONY: clean

clean:
	cd $(SRC_DIR) && $(MAKE) clean

