/**
 *  File credits.c
 *
 *  Implements credits.h. Creates and renders an end-game splash screen upon
 *  meeting the win conditions.
 *
 *  @author Ali Reza Sultani
 */
#include "credits.h"
#include "spritesheet.h"

struct Credits *Credits_initialize(SDL_Renderer *renderer) {
    struct Credits *credits;
    credits = (struct Credits*)malloc(sizeof(struct Credits));
    credits->renderer = renderer;
    credits->congrats = Spritesheet_create(CREDITS_CONGRATULATIONS,
                                           1, 1, 1, renderer);
    credits->cont = Spritesheet_create(CREDITS_CONTINUE, 1, 1, 1, renderer);
    return credits;
}

void Credits_run(struct Credits *credits) {
    SDL_Event e;
    SDL_SetRenderDrawColor(credits->renderer, 246, 192, 55, 0x00);
    SDL_RenderClear(credits->renderer);
    Spritesheet_render(credits->congrats, CONGRATS_X, CONGRATS_Y, 0);
    SDL_RenderPresent(credits->renderer);
    SDL_Delay(CONTINUE_DELAY);
    Spritesheet_render(credits->cont, CONTINUE_X, CONTINUE_Y, 0);
    SDL_RenderPresent(credits->renderer);

    // Clear the event queue and wait for a keyboard press.
    while (SDL_PollEvent(&e) != 0);
    do {
        SDL_WaitEvent(&e);
    } while (e.type != SDL_KEYDOWN);
}

void Credits_delete(struct Credits *credits) {
    if (credits != NULL) {
        Spritesheet_delete(credits->congrats);
        Spritesheet_delete(credits->cont);
        free(credits);
    }
}