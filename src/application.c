/**
 *  File application.c
 *
 *  Implements application.h. Creates and manages the main window screen unto
 *  which the game shall be ran.
 *
 *  @author Ali Reza Sultani
 */
#include "application.h"
#include "menu.h"
#include "game.h"
#include "credits.h"
#include <stdio.h>

struct Application *Application_initialize() {
    struct Application *application;

    // Initialize the SDL Library.
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
        fprintf(stderr, "SDL failed to initialize: %s\n", SDL_GetError());
        return NULL;
    }

    // Check rendering quality.
    if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
        fprintf(stderr, "Warning: Linear texture filtering not enabled!");
    }

    // Create the application window.
    application = (struct Application*)malloc(sizeof(struct Application));
    application->window = SDL_CreateWindow("Hungry Bob",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (application->window == NULL) {
        fprintf(stderr, "Window could not be created: %s\n", SDL_GetError());
        return NULL;
    }

    // Create the renderer.
    application->renderer = SDL_CreateRenderer(application->window, -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (application->renderer == NULL) {
        fprintf(stderr, "Renderer could not be created: %s\n", SDL_GetError());
        return NULL;
    }
    SDL_SetRenderDrawColor(application->renderer, 246, 192, 55, 0x00);

    // Check support for the PNG image formats.
    int imgFlags = IMG_INIT_PNG;
    if (!(IMG_Init(imgFlags) & imgFlags)) {
        fprintf(stderr, "SDL_image failed to initialize: %s\n", IMG_GetError());
        return NULL;
    }

    // Define the initial components of the application.
    application->menu = Menu_initialize(application->renderer);
    if (application->menu == NULL) {
        fprintf(stderr, "Failed to initialize menu: %s\n", IMG_GetError());
        return NULL;
    }
    application->state = APPLICATION_STATE_MENU;
    application->game = NULL;
    application->credits = NULL;

    return application;
}

void Application_run(struct Application *application) {
    while (application->state != APPLICATION_STATE_QUIT) {
        switch (application->state) {
            case APPLICATION_STATE_MENU:
                Menu_run(application->menu);

                // Check application state after menu selection.
                if (application->menu->choice == MENU_QUIT) {
                    application->state = APPLICATION_STATE_QUIT;
                } else if (application->menu->choice == MENU_PLAY) {
                    application->state = APPLICATION_STATE_PLAY;
                }
                break;
            case APPLICATION_STATE_PLAY:
                application->game = Game_initialize(application->renderer);
                Game_run(application->game);

                // Check application state after return from game module.
                if (application->game->state == GAMESTATE_QUIT) {
                    application->state = APPLICATION_STATE_QUIT;
                } else if (application->game->state == GAMESTATE_WIN) {
                    application->state = APPLICATION_STATE_CREDITS;
                    Game_delete(application->game);
                    application->game = NULL;
                } else if (application->game->state == GAMESTATE_RETURN) {
                    application->state = APPLICATION_STATE_MENU;
                    application->menu->choice = MENU_NONE;
                    Game_delete(application->game);
                    application->game = NULL;
                }
                break;
            case APPLICATION_STATE_CREDITS:
                application->credits = Credits_initialize(application->renderer);
                Credits_run(application->credits);

                // Return to the main menu when leaving the credits screen.
                application->state = APPLICATION_STATE_MENU;
                application->menu->choice = MENU_NONE;
                Credits_delete(application->credits);
                application->credits = NULL;
                break;
            case APPLICATION_STATE_QUIT:
                break;
        }
    }
}

void Application_shutDown(struct Application *application) {
    SDL_DestroyRenderer(application->renderer);
    SDL_DestroyWindow(application->window);
    Menu_delete(application->menu);
    Game_delete(application->game);
    Credits_delete(application->credits);
    free(application);
    application = NULL;
    IMG_Quit();
    SDL_Quit();
}