#include "character.h"
#include "spritesheet.h"
#include "animated_spritesheet.h"
#include "level.h"

struct Character *Character_initialize(SDL_Renderer *renderer){
    struct Character *character;
    character = (struct Character*)malloc(sizeof(struct Character));
    character->visual = AnimatedSpritesheet_create(CHAR_FILENAME, 6, 32, 192, 30, renderer);
    character->x = 50;
    character->y = 640;
    character->renderer = renderer;
    character->state = BOB_IDLE;
    AnimatedSpritesheet_render(character->visual, character->x, character->y);
    AnimatedSpritesheet_setRow(character->visual, 2);
    SDL_RenderPresent(renderer);
    return character;
}

void Character_move(struct Game *game) {
    switch (game->character->state) {
        case BOB_JUMPING_RIGHT:
            // Animates Bob to a squatting position.
            for (int j = 0; j < JUMP_ANIMATION; j++) {
                Game_render(game);
            }

            // Bob jumps to the right.
            for (int i = 0; i < JUMP_HEIGHT; i++) {
                if (!isWall(game, DIRECTION_RIGHT))
                    game->character->x += 1;
                if (!isWall(game, DIRECTION_UP))
                    game->character->y -= 2;
                isDonut(game);
                Game_render(game);
            }

            // Bob descends towards the ground.
            while (!isWall(game, DIRECTION_DOWN)) {
                if (!isWall(game, DIRECTION_RIGHT))
                    game->character->x += 1;
                game->character->y += 2;
                isDonut(game);
                Game_render(game);
            }
            break;
        case BOB_JUMPING_LEFT:
            // Animates Bob to a squatting position.
            for (int j = 0; j < JUMP_ANIMATION; j++) {
                Game_render(game);
            }

            // Bob jumps to the left.
            for (int i = 0; i < JUMP_HEIGHT; i++) {
                if (!isWall(game, DIRECTION_LEFT))
                    game->character->x -= 1;
                if (!isWall(game, DIRECTION_UP))
                    game->character->y -= 2;
                isDonut(game);
                Game_render(game);
            }

            // Bob descends towards the ground.
            while (!isWall(game, DIRECTION_DOWN)) {
                if (!isWall(game, DIRECTION_LEFT))
                    game->character->x -= 1;
                game->character->y += 2;
                isDonut(game);
                Game_render(game);
            }
            break;
        case BOB_JUMPING_VERTICALLY:
            // Animates Bob to a squatting position.
            for (int j = 0; j < JUMP_ANIMATION; j++) {
                Game_render(game);
            }
            
            // Bob jumps in a straight line.
            for (int i = 0; i < JUMP_HEIGHT; i++) {
                if (!isWall(game, DIRECTION_UP))
                    game->character->y -= 2;
                isDonut(game);
                Game_render(game);
            }
            break;
        case BOB_WALKING_RIGHT:
            if (!isWall(game, DIRECTION_RIGHT))
                game->character->x += 2;
            isDonut(game);
            Game_render(game);
            break;
        case BOB_WALKING_LEFT:
            if (!isWall(game, DIRECTION_LEFT))
                game->character->x -= 2;
            isDonut(game);
            Game_render(game);
            break;
        default:
            break;
    }
}

void Character_delete(struct Character *character) {
    if (character != NULL) {
        AnimatedSpritesheet_delete(character->visual);
        free(character);
    }
}

void Char_direction(enum BobState state, struct Game *game) {
    if (game->character->state != state) {
        game->character->state = state;
        if (state == BOB_WALKING_RIGHT) AnimatedSpritesheet_setRow(game->character->visual, 0);
        if (state == BOB_WALKING_LEFT) AnimatedSpritesheet_setRow(game->character->visual, 1);
        if (state == BOB_IDLE) AnimatedSpritesheet_setRow(game->character->visual, 2);
        if (state == BOB_JUMPING_VERTICALLY) AnimatedSpritesheet_setRow(game->character->visual, 3);
        if (state == BOB_JUMPING_RIGHT) AnimatedSpritesheet_setRow(game->character->visual, 4);
        if (state == BOB_JUMPING_LEFT) AnimatedSpritesheet_setRow(game->character->visual, 5);
    }
}
