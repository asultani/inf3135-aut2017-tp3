/**
 *  File game.c
 *
 *  Implements the structures and functions for the main game framework.
 *  Encapsulates the two other major components: the character and the map.
 *
 *  @author Ali Reza Sultani
 */
#include "game.h"
#include "level.h"
#include "character.h"
#include "spritesheet.h"
#include "animated_spritesheet.h"

struct Game *Game_initialize(SDL_Renderer *renderer) {
    struct Game *game;
    game = (struct Game*)malloc(sizeof(struct Game));
    game->renderer = renderer;
    game->level = Level_initialize(game->renderer);
    game->character = Character_initialize(game->renderer);
    game->state = GAMESTATE_PLAY;
    return game;
}

void Game_run(struct Game *game) {
    SDL_Event e;
    const Uint8 *state = SDL_GetKeyboardState(NULL);
    AnimatedSpritesheet_run(game->character->visual);

    while (game->state == GAMESTATE_PLAY) {
        while (SDL_PollEvent(&e)) {
            state = SDL_GetKeyboardState(NULL);
            if (e.type == SDL_QUIT) {
                game->state = GAMESTATE_QUIT;
            } else {
                if (state[SDL_SCANCODE_ESCAPE]) {
                    game->state = GAMESTATE_RETURN;
                } else if (state[SDL_SCANCODE_RIGHT] && state[SDL_SCANCODE_UP]) {
                    Char_direction(BOB_JUMPING_RIGHT, game);
                } else if (state[SDL_SCANCODE_LEFT] && state[SDL_SCANCODE_UP]) {
                    Char_direction(BOB_JUMPING_LEFT, game);
                } else if (state[SDL_SCANCODE_UP]) {
                    Char_direction(BOB_JUMPING_VERTICALLY, game);
                } else if (state[SDL_SCANCODE_LEFT]) {
                    Char_direction(BOB_WALKING_LEFT, game);
                } else if (state[SDL_SCANCODE_RIGHT]) {
                    Char_direction(BOB_WALKING_RIGHT, game);
                } else {
                    Char_direction(BOB_IDLE, game);
                }
            }
            Character_move(game);

            // Gravitational Pull
            while (!isWall(game, DIRECTION_DOWN)) {
                game->character->y += 1;
                isDonut(game);
                Game_render(game);
            }

            // Check for win conditions
            if (game->level->donutsRemaining == 0)
                game->state = GAMESTATE_WIN;
        }
        Game_render(game);
    }
}

void Game_render(struct Game *game) {
    Spritesheet_render(game->level->map, 0, 0, 0);

    // Rendering only the donuts that are still remaining.
    for (int i = 0; i < DONUTS_INIT_COUNT; i++) {
        if (game->level->donuts[i] != NULL) {
            AnimatedSpritesheet_render(game->level->donuts[i],
                                       DONUTS_XY[i][0],
                                       DONUTS_XY[i][1]);
        }
    }

    AnimatedSpritesheet_render(game->character->visual,
                               game->character->x,
                               game->character->y);
    SDL_RenderPresent(game->renderer);
}

void Game_delete(struct Game *game) {
    if (game != NULL) {
        Character_delete(game->character);
        Level_delete(game->level);
        free(game);
    }
}