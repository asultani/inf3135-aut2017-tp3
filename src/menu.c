/**
 *  File menu.c
 *  
 *  Implements menu.h. Implements the main menu screen.
 *  
 *  @author Ali Reza Sultani
 */
#include "menu.h"
#include "spritesheet.h"

struct Menu *Menu_initialize(SDL_Renderer *renderer) {
    struct Menu *menu;
    menu = (struct Menu*)malloc(sizeof(struct Menu));
    menu->renderer = renderer;
    menu->choice = MENU_NONE;
    menu->title = Spritesheet_create(TITLE_FILENAME, 1, 1, 1, renderer);
    menu->options = Spritesheet_create(OPTIONS_FILENAME, 1, 1, 1, renderer);
    return menu;
}

void Menu_run(struct Menu *menu) {
    SDL_Event e;

    while (menu->choice == MENU_NONE) {
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                menu->choice = MENU_QUIT;
            } else if (e.type == SDL_KEYDOWN) {
                switch (e.key.keysym.sym) {
                    case SDLK_1:
                        menu->choice = MENU_PLAY;
                        break;
                    case SDLK_2:
                        menu->choice = MENU_QUIT;
                        break;
                }
            }
        }

        SDL_RenderClear(menu->renderer);
        Spritesheet_render(menu->title, TITLE_X, TITLE_Y, 0);
        Spritesheet_render(menu->options, OPTIONS_X, OPTIONS_Y, 0);
        SDL_RenderPresent(menu->renderer);
    }
}

void Menu_delete(struct Menu *menu) {
    if (menu != NULL) {
        Spritesheet_delete(menu->title);
        Spritesheet_delete(menu->options);
        free(menu);
    }
}