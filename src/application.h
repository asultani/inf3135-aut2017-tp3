/**
 *  File application.h
 *
 *  Implements the data structure and functions for the application window.
 *
 *  @author Ali Reza Sultani
 */
#ifndef APPLICATION_H
#define APPLICATION_H

#include <SDL.h>
#include <SDL_image.h>

// --------- //
// Constants //
// --------- //

#define SCREEN_WIDTH  900
#define SCREEN_HEIGHT (SCREEN_WIDTH * 4 / 5)

// --------------- //
// Data structures //
// --------------- //

enum ApplicationState {
    APPLICATION_STATE_MENU,     // Main Menu
    APPLICATION_STATE_PLAY,     // In-Game
	APPLICATION_STATE_CREDITS,  // Credit Screen
    APPLICATION_STATE_QUIT      // Quit
};

struct Application {
    enum ApplicationState state; // The current state
    struct Menu *menu;           // The home menu
    struct Game *game;           // The current game
	struct Credits *credits;	 // The credits screen
    SDL_Window* window;          // The window
    SDL_Renderer* renderer;      // The renderer
};

// --------- //
// Functions //
// --------- //

/**
 * Creates a new application.
 *
 * @return  A pointer to a new application, NULL if there was an error
 */
struct Application *Application_initialize();

/**
 * Start running the application.
 *
 * @param application  The application to run
 */
void Application_run(struct Application *application);

/**
 * Closes the given application.
 *
 * @param application  The application to be closed
 */
void Application_shutDown(struct Application *application);

#endif
