/**
 *  File level.h
 *
 *  Implements the structures and functions for the gaming environment.
 *  Specifically, the module manages the background map, its obstacles
 *  and
 *
 *  @author Ali Reza Sultani
 */
#ifndef LEVEL_H
#define LEVEL_H

#include <stdbool.h>
#include <SDL.h>
#include <SDL_image.h>
#include "character.h"
#include "game.h"

// --------- //
// Constants //
// --------- //

#define MAP_FILENAME "../assets/map.png"
#define DONUTS_FILENAME "../assets/donut1.png"
#define DONUTS_SPRITE_WIDTH 32
#define DONUTS_SPRITE_HEIGHT 32
#define DONUTS_INIT_COUNT 3
const int DONUTS_XY[DONUTS_INIT_COUNT][2];

// --------------- //
// Data structures //
// --------------- //

struct Level {
	struct Spritesheet *map; // The map sprite.
	struct AnimatedSpritesheet *donuts[DONUTS_INIT_COUNT]; // The donuts sprite.
	Uint32 wall;             // Sample pixel from a wall.
	Uint32 space;            // Sample pixel of an empty space.
	int donutsRemaining;     // Number of donuts left.
	SDL_Surface *surface;    // The map surface.
	SDL_Renderer *renderer;  // The renderer.
};

// --------- //
// Functions //
// --------- //

/**
 *  Creates the level environment composed of :
 *   - A Background map.
 *   - Donuts.
 *
 *  @param  renderer  The renderer.
 *  @return           Void.
 */
struct Level *Level_initialize(SDL_Renderer *renderer);

/**
 *  Deletes the level.
 *
 *  @param  level  The level to delete.
 *  @return        Void.
 */
void Level_delete(struct Level *level);

/**
 *  Checks in the given direction, from the perspective of the main
 *  character, if there is a wall. (Pixel based collision detection.)
 *
 *  @param  game       The game in which the check is to be done.
 *  @param  direction  The direction to check.
 *  @return            True if the direction is obstructed by a wall.
 */
bool isWall(struct Game *game, enum Direction direction);

/**
 *  Checks whether the character captured a donut, and if so, update the
 *  environment by :
 *  - Deleting the captured donut from the sprites.
 *  - Updating the number of donuts left.
 *  (Rectangle based collision detection.)
 *
 *  @param  game  The game in which the check/update is to be done.
 *  @return       Void.
 */
void isDonut(struct Game *game);

#endif