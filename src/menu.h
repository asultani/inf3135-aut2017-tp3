/**
 *  File menu.h
 *  
 *  Implements the structures and functions required to create 
 *  the main menu screen.
 *  
 *  @author Ali Reza Sultani
 */
#ifndef MENU_H
#define MENU_H

#include <SDL.h>
#include <SDL_image.h>
#include "application.h"

// --------- //
// Constants //
// --------- //

#define TITLE_FILENAME "../assets/menu_title.png"
#define TITLE_WIDTH 800
#define TITLE_X ((SCREEN_WIDTH - TITLE_WIDTH) / 2)
#define TITLE_Y 0
#define OPTIONS_FILENAME "../assets/menu_options.png"
#define OPTIONS_WIDTH 800
#define OPTIONS_X ((SCREEN_WIDTH - OPTIONS_WIDTH) / 2)
#define OPTIONS_Y 300

// --------------- //
// Data structures //
// --------------- //

enum MenuChoice {
    MENU_NONE, // User has not made a choice
    MENU_QUIT, // User wishes to quit
    MENU_PLAY  // User wishes to play
};

struct Menu {
    enum MenuChoice choice;    // The choice of the user
    struct Spritesheet *title; // The title sprite
    struct Spritesheet *options;  // The play sprite
    SDL_Renderer *renderer;    // The renderer
};

// --------- //
// Functions //
// --------- //

/**
 * Creates a new menu.
 *
 * @param renderer  The renderer
 * @return          A pointer to the menu, NULL if there was an error
 */
struct Menu *Menu_initialize(SDL_Renderer *renderer);

/**
 * Start running the menu.
 *
 * @param menu  The menu to show
 */
void Menu_run(struct Menu *menu);

/**
 * Delete the menu.
 *
 * @param menu  The menu to delete
 */
void Menu_delete(struct Menu *menu);

#endif
