/**
 *  File character.h
 *
 *  Implements the structures and functions for the main character Bob.
 *
 *  @author Ali Reza Sultani
 */
#ifndef CHARACTER_H
#define CHARACTER_H

#include <SDL.h>
#include <SDL_image.h>
#include "game.h"

// --------- //
// Constants //
// --------- //

#define CHAR_FILENAME "../assets/character1.png"
#define CHARACTER_PADDING 10
#define JUMP_ANIMATION 30
#define JUMP_HEIGHT 70

// --------------- //
// Data structures //
// --------------- //

enum Direction {     // Elementary directions
    DIRECTION_RIGHT, // Right
    DIRECTION_UP,    // Up direction
    DIRECTION_LEFT,  // Left direction
    DIRECTION_DOWN,  // Down direction
};

enum BobState {
    BOB_IDLE, 				// Bob is idle
    BOB_WALKING_RIGHT, 		// Bob is walking to the right
    BOB_WALKING_LEFT, 		// Bob is walking to the left
    BOB_JUMPING_VERTICALLY, // Bob is jumping vertically
    BOB_JUMPING_RIGHT, 		// Bob is jumping to the right
    BOB_JUMPING_LEFT, 		// Bob is jumping to the left
};

struct Character {
	struct AnimatedSpritesheet *visual; // Character sprites
	enum BobState state;                // Current movement state.
	int x;                              // The X-axis position of Bob.
	int y;                              // The Y-axis position of Bob.
	SDL_Texture *texture;               // The character texture.
	SDL_Renderer *renderer;             // The renderer.
};

// --------- //
// Functions //
// --------- //

/**
 *  Creates the main character.
 *
 *  @param  renderer  The renderer.
 *  @return           A pointer to the main character.
 */
struct Character *Character_initialize(SDL_Renderer *renderer);

/**
 *  Moves and renders the character towards it's current state.
 *
 *  @param  game       The game containing the character.
 *  @return            Void.
 */
void Character_move(struct Game *game);

/**
 *  Deletes the character.
 *
 *  @param  character  The character to delete.
 *  @return            Void.
 */
void Character_delete(struct Character *character);

/**
 *  Sets the character's main state of motion for rendering purposes.
 *  This state will be used to determine the correct row number within
 *  the spritesheet that corresponds with the given motion.
 *
 *  @param  state  The requested state.
 *  @param  game   The game containing the character.
 *  @return        Void.
 */
void Char_direction(enum BobState state, struct Game *game);

#endif