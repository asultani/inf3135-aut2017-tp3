/**
 *  File game.h
 *
 *  Implements the structures and functions for the main game framework.
 *  Encapsulates the two other major components: the character and the map.
 *
 *  @author Ali Reza Sultani
 */
#ifndef GAME_H
#define GAME_H

#include <SDL.h>
#include <SDL_image.h>

// --------------- //
// Data structures //
// --------------- //

enum GameState {
	GAMESTATE_PLAY,     // Currently playing.
	GAMESTATE_WIN,      // All donuts have been collected.
    GAMESTATE_RETURN,   // Player quits to Main Menu.
	GAMESTATE_QUIT,     // Player quits to Desktop.
};

struct Game {
	struct Character *character;    // The character.
	struct Level *level;            // The map/level.
	enum GameState state;           // The current game state.
	SDL_Renderer *renderer;         // The renderer.
};

// --------- //
// Functions //
// --------- //

/**
 *  Initializes the game along with its subcomponents: the map and character.
 *
 *  @param  renderer  The renderer.
 *  @return           A pointer to the game structure.
 */
struct Game *Game_initialize(SDL_Renderer *renderer);

/**
 *  Displays the game screen.
 *
 *  @param  game  The game to display.
 *  @return       Void.
 */
void Game_run(struct Game *game);

/**
 *  Deletes the game.
 *
 *  @param  game  The game to delete.
 *  @return       Void.
 */
void Game_delete(struct Game *game);

/**
 *  Renders the game to the application window.
 *
 *  @param  game  The game to render.
 *  @return       Void.
 */
void Game_render(struct Game *game);

#endif