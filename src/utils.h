/**
 *  File utils.h
 *
 *  Implements various utility function that do not necessarily belong
 *  to a specific component or module of the game.
 *
 *  @author Ali Reza Sultani
 */
#ifndef UTILS_H
#define UTILS_H

#include <SDL.h>
#include <SDL_image.h>

/**
 *  Get the unsigned 32 bit integer representing the color definition
 *  of a pixel at a specified location within a specified surface.
 *
 *  @param  surface  The surface to scan.
 *  @param  x        The X-axis coordinate for the requested pixel.
 *  @param  y        The Y-axis coordinate for the requested pixel.
 *  @return          The color definition of the pixel.
 */
Uint32 getpixel(SDL_Surface *surface, int x, int y);

#endif