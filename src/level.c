/**
 *  File level.c
 *
 *  Implements level.h. Manages the main environment in which the game runs.
 *
 *  @author Ali Reza Sultani
 */
#include "level.h"
#include "spritesheet.h"
#include "animated_spritesheet.h"
#include "utils.h"

// Donuts Position Coordinates Constants
const int DONUTS_XY[DONUTS_INIT_COUNT][2] = { {50 , 50},      // Donut #1
                                              {580, 520},     // Donut #2
                                              {800, 50} };    // Donut #3

struct Level *Level_initialize(SDL_Renderer *renderer) {
    struct Level *level;
    level = (struct Level*)malloc(sizeof(struct Level));
    level->renderer = renderer;

    // Map Initialization
    level->surface = IMG_Load(MAP_FILENAME);
    level->map = Spritesheet_create(MAP_FILENAME, 1, 1, 1, renderer);
    level->wall = getpixel(level->surface, 1, 1);
    level->space = getpixel(level->surface, 50, 50);
    Spritesheet_render(level->map, 0, 0, 0);

    // Donuts Initialization
    for (int i = 0; i < 3; i++) {
        level->donuts[i] = AnimatedSpritesheet_create(DONUTS_FILENAME, 
                                                      1, 32, 32, 30, 
                                                      renderer);
        AnimatedSpritesheet_run(level->donuts[i]);
    }
    level->donutsRemaining = DONUTS_INIT_COUNT;

    return level;
}

void Level_delete(struct Level *level) {
    if (level != NULL) {
        Spritesheet_delete(level->map);
        for (int i = 0; i < DONUTS_INIT_COUNT; i++) {
            AnimatedSpritesheet_delete(level->donuts[i]);
        }
        SDL_FreeSurface(level->surface);
        free(level);
    }
}

bool isWall(struct Game *game, enum Direction direction) {
    SDL_Surface *surface = game->level->surface;
    int posx = game->character->x;
    int posy = game->character->y;
    int width = game->character->visual->spritesheet->spriteWidth;
    int height = game->character->visual->spritesheet->spriteHeight;
    Uint32 wall = game->level->wall;
    bool isWall = false;

    switch (direction) {
        case DIRECTION_RIGHT:
            for (int i = 0; i < height; i += 5) {
                if (getpixel(surface, posx + width + 5 - CHARACTER_PADDING, 
                             posy + i) == wall)
                    return true;
            }
            break;
        case DIRECTION_LEFT:
            for (int i = 0; i < height; i += 5) {
                if (getpixel(surface,  posx - 5 + CHARACTER_PADDING, 
                             posy + i) == wall)
                    return true;
            }
            break;
        case DIRECTION_UP:
            for (int i = 0; i < (width - 2 * CHARACTER_PADDING); i += 5) {
                if (getpixel(surface,  posx + i + CHARACTER_PADDING, 
                             posy - 5) == wall)
                    return true;
            }
            break;
        case DIRECTION_DOWN:
            for (int i = 0; i < (width - 2 * CHARACTER_PADDING); i += 5) {
                if (getpixel(surface,  posx + i + CHARACTER_PADDING, 
                             posy + height + 5) == wall)
                    return true;
            }
            break;
    }

    return isWall;
}

void isDonut(struct Game *game) {
    SDL_Rect donut;
    SDL_Rect character;

    // Set the collision box for the characters current position and size.
    character.x = game->character->x;
    character.y = game->character->y;
    character.w = game->character->visual->spritesheet->spriteWidth;
    character.h = game->character->visual->spritesheet->spriteHeight;

    // Set the collision box size for the donuts.
    donut.w = DONUTS_SPRITE_WIDTH;
    donut.h = DONUTS_SPRITE_HEIGHT;

    for (int i = 0; i < DONUTS_INIT_COUNT; i++) {
        if (game->level->donuts[i] != NULL) {
            // Set the collision box coordinates for each remaining donuts.
            donut.x = DONUTS_XY[i][0];
            donut.y = DONUTS_XY[i][1];

            // Check if the donut and the character are colliding.
            if (SDL_HasIntersection(&donut, &character)) {
                AnimatedSpritesheet_delete(game->level->donuts[i]);
                game->level->donuts[i] = NULL;
                game->level->donutsRemaining--;
            }
        }
    }
}