/**
 *  File credits.h
 *
 *  Implements the structures and functions for the end game credits module.
 *
 *  @author Ali Reza Sultani
 */
#ifndef CREDITS_H
#define CREDITS_H

#include <SDL.h>
#include <SDL_image.h>
#include "application.h"

// --------- //
// Constants //
// --------- //

#define CREDITS_CONGRATULATIONS "../assets/credits_congrats.png"
#define CREDITS_CONTINUE "../assets/credits_continue.png"
#define CONGRATS_X 50
#define CONGRATS_Y 0
#define CONTINUE_X 50
#define CONTINUE_Y 300
#define CONTINUE_DELAY (3 * 1000)

// --------------- //
// Data structures //
// --------------- //

struct Credits {
	struct Spritesheet *congrats;   // Congrats sprite
	struct Spritesheet *cont;       // Continue sprite
	SDL_Renderer *renderer;         // Renderer
};

// --------- //
// Functions //
// --------- //

/**
 *  Creates the splash screen for the credits.
 *
 *  @param  renderer  The renderer.
 *  @return           A pointer to the credits screen.
 */
struct Credits *Credits_initialize(SDL_Renderer *renderer);

/**
 *  Displays the endgame credits.
 *
 *  @param  credits  The credits screen to show.
 *  @return          Void.
 */
void Credits_run(struct Credits *credits);

/**
 *  Deletes the endgame credits.
 *
 *  @param  credits  The credits screen to delete.
 *  @return          Void.
 */
void Credits_delete(struct Credits *credits);

#endif