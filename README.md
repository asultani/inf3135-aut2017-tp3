# Project 3

## Description

The project is a platformer videogame inspired by one of gaming's greatest masterpiece: *Super Mario Bros*.

The game is setout in a maze type level. It consists of guiding the main character "*Bob*", a hungry man with a sweet tooth, to collect all the donuts that are layed out through the level.

The main purpose of this project was to put in practice all the notions that are were taught through out the semester, mainly the professional aspect of software development and it's maintenance, along with the use of a graphical C library (*SDL2*).

This project was completed as part of an assignment for the INF3135 course (group 40) at the Université du Québec à Montréal, and was completed under the direction of professor Alexandre Terrasa (@Morriar) in the Fall session of 2017.

## Author

**NOM**: Ali Reza Sultani

## Gameplay

> **Note:**
>
> The game is launched inside a **900 x 720** resolution window.  For such reasons, it is recommended that the user has a desktop resolution greater or equal to *1024 x 768* in order to have a game window that fits in your screen.
> 
> See also the Install section on how to properly install and launch the application.

### Menu

Upon launching the game, the user will be introduced to the main menu whom will present the user with two choices :

![menu](https://preview.ibb.co/eGxCPw/SS7.jpg)

```
1 : To Start a New Game.
2 : Quit to Desktop.
```

The user will need to make a selection by pressing the appropriate key on his keyboard.

### Gameplay

When the user selects the option `1` from the main menu, he will be presented to the following screen:

![game](https://preview.ibb.co/bx4Crb/SS5.jpg)

The purpose of the game is to navigate through the maze-like level and capture all the tasty donuts. A donut is automatically captured when Bob moves unto the donuts. The user will be presented with 2 gameplay challenges:

- **Walls :**  Bob cannot move through the dark colored walls. Regular old Bob unfortunately doesn't have the same powers as [*Shadowcat*](https://en.wikipedia.org/wiki/Kitty_Pryde) from the *X-Men*. The user will need to put effort into navigating around them in order to reach the objectives.
- **Gravity :** Bob is consistently pulled down by the gravitational forces. Bob will need to make calculated jumps in order to climb the platforms.

The keyboard controls in the game screen are as follows:

```
--- SINGLE KEY PRESS ---
LEFT ARROW  : Move Bob to the left.
RIGHT ARROW : Move Bob to the right.
UP ARROW    : Make Bob jump vertically.
--- MULTI KEY PRESS ---
UP + RIGHT  : Make Bob jump to the right.
UP + LEFT   : Make Bob jump to the left.

ESC         : Quit the current game to menu.
```

### Credits

Congratulations you finished the level! Upon successfully capturing all the donuts, the user will be brought to an end-game screen. 

![CREDITS](https://preview.ibb.co/fZshrb/SS6.jpg)

After a small delay, the user will be prompted to press any key in order to return to the main menu from which he can decide to play again or to quit.

### Additional Maps

The game relies on *color based* pixel collisions rather than *rectangle based* pixel collisions. This makes the process of adding additional maps extremely easy. The user could take a few minutes and create a new *900 x 720* map in *MS Paint* or *Photoshop* and add it to the game in instants. Upon initialization, the game will automatically sample a pixel in the border as a `wall` pixel and another pixel slightly off the upper edge as an empty space. When the character moves, the game automatically validates the move by comparing the pixels at the anticipated location. Furthermore, the user may need to modify a few constants:

- The starting location of the character, if necessary.
- The positions of the donuts (or even adding new ones).
- The filename constant for the new map.

## Supported Platforms

This program was developped and tested on a *Ubuntu 16.04.3 LTS* machine.

## Dependencies

- [SDL 2.0](https://wiki.libsdl.org/Installation) - Simple DirectMedia Layer is a cross-platform development library designed to provide low level access to audio, keyboard, mouse, joystick, and graphics hardware via OpenGL and Direct3D. 

## Installation

The program can be compiled by running the following command from the root or the `/src/` folder of the project :

```
make
```

After the compilation the user can then run the the program from the `/src/` folder by typing :

```
./bob
```

It's important to note that running the program from any folder other than `/src/` will cause the game to start incorrectly. Please make sure to verify that you are in the correct folder before launching the game and if not to `cd src` prior to the launch.

Furthermore, it is important to note that the game is launched inside a 900 x 720 window. The game was coded for a smaller resolution because it facilitates not only the development process, but also prevents users with low resolution screens to resize their desktops. If you are among the unlikely few who still runs a desktop resolution below 1024 x 768, you might have to resize your desktop resolution so that the application can fit properly to your screen.

## References

- [SDL2 Wiki](https://wiki.libsdl.org/FrontPage) - The SDL2 Wiki is the documentation for the graphical library used through out this project.
- [Maze-SDL](https://bitbucket.org/ablondin-projects/maze-sdl) - The Maze game that is coded by Alexandre Blondin-Masse (@ablondin) has been relied upon in order to complete the current project. The Maze game provided a footprint into properly grasping the different functionality of the SDL library. Furthermore, it is important to note that the following modules have been imported as is into the current project : 

    - `animated_spritesheets` 
    - `spritesheets`
    - `main`
 
- [Assets](https://gitlab.com/ablondin/inf3135-aut2017-tp3-assets) - The animated sprites representing the donuts and the character have been provided by Alexandre Blondin-Masse (ablondin).
- [Stackoverflow](https://stackoverflow.com/) - The Stackoverflow community has been a terrific source of quick information with respect to the usage of Git and C Code.
- [INF3135 Class Notes and Lectures](http://moz-code.org/uqam/cours/INF3135/) - The class notes and lectures of Alexandre Terrasa (@morriar) have been used as a guideline for the conception of various component of this project. 

## Task List

In order to facilitate the development process, the project has been divided in 4 main components.

- [x] TASK 1: Adding the main menu.
- [x] TASK 2: Adding the game framework.
	- [x] TASK 2.1: Adding gravity to the game.
	- [x] TASK 2.2: Integrating the animated sprites.
- [x] TASK 3: Adding the credits screen.
- [x] TASK 4: Documentation and refactoring.

Unexpected Changes:

- FIX #1: Revamped the movement system.

In terms of unexpected changes, the `FIX #1` component consisted of a major revamp of `TASK #2`. As time allowed it, the entire movement system was recoded in order to provide the user with a better gaming experience. The keys are more responsive, and the game renders properly the different character state unlike the first instance.

There were also other various changes/fixes that were brought in directly to the `master` branch as a final clean up measure. As these changes were all mostly unrelated and minor, these changes did not warrant a branch of it's own.

## Status

The project has been completed.